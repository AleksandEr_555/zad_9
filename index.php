<?php

abstract class Product {

    public $name;
    public $cena;
    public $massa;
    public $image;

    public static $companyNameInfo = 'Aleksandr';
    const YEAR_START = 2000;

    public static function showCompanyInfo()
    {
        echo 'Название компании: ' . self::$companyNameInfo . "<br>";
        echo 'Год зарождения компании: ' . Product::YEAR_START;
    }
    public function __construct($name, $cena, $massa, $image) {

        $this->name = $name;
        $this->cena = $cena;
        $this->massa = $massa;
        $this->image = $image;
    }

    public function ProductPrint()
    {
        echo "<h3>$this->name: $this->massa, $this->cena</h3><hr>";
    }

    public function ShowImage()
    {
        echo "<div style='width: 100px; height: 100px; background-image: url($this->image)'></div>";
    }
}

class Chocolate extends Product
{
    public $kal;

    public function __construct($name, $cena, $massa, int $kal, $image)
    {
        $this->kal = $kal;
        parent::__construct($name, $cena, $massa, $image);
    }

    public function ProductPrint()
    {
        echo "<h3>$this->name: $this->massa, $this->cena, $this->kal kkal</h3>";
    }

    public function ShowImage()
    {
        echo "<div style='width: 200px; height: 200px; background-image: url($this->image)'></div><hr>";
    }
}

class Candy extends Product
{
    public function __construct($name, $cena, $massa, $image)
    {
        parent::__construct($name, $cena, $massa, $image);
    }

    public function ProductPrint()
    {
        echo "<h3>$this->name: $this->massa, $this->cena</h3>";
    }

    public function ShowImage()
    {
        echo "<div style='width: 100px; height: 100px; background-image: url($this->image)'></div><hr>";
    }
}

$choco = new Chocolate('Алёнка', '50 p.', '100g', 1000, 'chocolate.jpg');
$konf = new Candy('Маковка', '50 p.', '20g', 'candy.jpg');

?>

<html>
<body>
<center>
    <div style='width: 700px; height: 100px; margin-top: 10px; border: 2px solid black; background-color: orange'>
        <h1><b>Домашнее задание №9</b></h1>
    </div>
    <div style='width: 700px; height: 700px; margin-top: 10px; border: 2px solid black; background-color: #F0E68C'>
        <?php
        $choco->ProductPrint();
        $choco->ShowImage();

        $konf->ProductPrint();
        $konf->ShowImage();
        ?>
        <h3><b>Вывод статического свойства и метода с помощью обращения к Product</b></h3>
        <?php
        Product::showCompanyInfo();
        ?>
        <h3><b>Вывод статического свойства и метода с помощью объекта дочернего класса</b></h3>
        <?php
        $choco::showCompanyInfo();
        ?>
    </div>
</center>
</body>
</html>